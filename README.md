# Android-Halado-Negyedik

A videó során meglátogatott oldalak

## Global
* Android dashboard https://developer.android.com/about/dashboards/
* Google maps api key https://developers.google.com/maps/documentation/android-sdk/signup
* Sensorok lehetséges értékei https://developer.android.com/reference/android/hardware/SensorEvent#values
* Sensorokról - developer oldal - https://developer.android.com/guide/topics/sensors/

## Libek, komponensek
* Support libek https://developer.android.com/topic/libraries/support-library/packages
* Constraint layout https://developer.android.com/training/constraint-layout/
* Guideline https://developer.android.com/reference/android/support/constraint/Guideline

## Guide oldalak

* Guideline percentage... https://stackoverflow.com/questions/41085338/change-guideline-percentage-in-constraint-layout-programmatically
* Szuper location guide https://www.androidhive.info/2012/07/android-gps-location-manager-tutorial/
* Runtime permissions https://developer.android.com/training/permissions/requesting
* Play services setup https://developers.google.com/android/guides/setup
* Sensors tutorial https://www.tutorialspoint.com/android/android_sensors.htm
* Sensors - vogella http://www.vogella.com/tutorials/AndroidSensor/article.html
* Current location https://developer.android.com/training/location/retrieve-current

## Egyéb

* Említett szenzor lib https://github.com/nisrulz/sensey