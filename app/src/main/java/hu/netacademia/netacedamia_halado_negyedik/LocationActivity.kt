package hu.netacademia.netacedamia_halado_negyedik

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import java.text.SimpleDateFormat
import java.util.*

class LocationActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {
        val ID_TAG = LocationActivity::class.java.simpleName
    }

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    private var latitudeText: TextView? = null
    private var longitudeText: TextView? = null
    private var mapView: MapView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        initViews()
        getLastKnownLocation()
        initLocationCallBack()
        initMaps(savedInstanceState)
    }

    private fun initViews() {
        latitudeText = findViewById(R.id.location_latitude)
        longitudeText = findViewById(R.id.location_longitude)
        mapView = findViewById(R.id.location_maps)
    }

    private fun getLastKnownLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.lastLocation.addOnSuccessListener { lastKnownLocation ->
                handleLastLocation(lastKnownLocation)
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    0)
        }
    }

    private fun handleLastLocation(lastKnownLocation: Location?) {
        lastKnownLocation?.let { validLocation ->
            latitudeText?.text = validLocation.latitude.toString()
            longitudeText?.text = validLocation.longitude.toString()
        }
    }

    private fun initLocationCallBack() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                locationResult.locations.forEach { location ->
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val locationDate = dateFormat.format(Date(location.time))
                    Log.i(ID_TAG, locationDate)
                }
            }
        }
    }

    private fun initMaps(savedInstanceState: Bundle?) {
        mapView?.onCreate(savedInstanceState)

        MapsInitializer.initialize(this)
        mapView?.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map?.isMyLocationEnabled = true
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        fusedLocationProviderClient.requestLocationUpdates(LocationRequest.create(), locationCallback, null)

        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)

        mapView?.onPause()
    }
}