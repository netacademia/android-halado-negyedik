package hu.netacademia.netacedamia_halado_negyedik

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView

class SensorActivity : AppCompatActivity(), SensorEventListener {

    companion object {
        val ID_TAG = SensorActivity::class.java.simpleName
    }

    private var lightSensorText: TextView? = null
    private var proximitySensorText: TextView? = null

    private var sensorManager: SensorManager? = null
    private var lightSensor: Sensor? = null
    private var proximitySensor: Sensor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sensor)

        initViews()
        initSensors()
        getSensors()
    }

    private fun initViews() {
        lightSensorText = findViewById(R.id.sensor_light)
        proximitySensorText = findViewById(R.id.sensor_proximity)
    }

    private fun initSensors() {
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        lightSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_LIGHT)
        proximitySensor = sensorManager?.getDefaultSensor(Sensor.TYPE_PROXIMITY)
    }

    private fun getSensors() {
        sensorManager?.getSensorList(Sensor.TYPE_ALL)?.forEach { sensor ->
            Log.i(ID_TAG, sensor.name)
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val sensor: Sensor = event?.sensor!!

        when {
            sensor.type == Sensor.TYPE_LIGHT -> {
                lightSensorText?.text = "${event?.values?.get(0)} lux"
            }
            sensor.type == Sensor.TYPE_PROXIMITY -> {
                proximitySensorText?.text = "${event?.values?.get(0)} cm"
            }
        }
    }

    override fun onResume() {
        super.onResume()
        sensorManager?.registerListener(this, lightSensor!!, SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager?.registerListener(this, proximitySensor!!, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager?.unregisterListener(this)
    }
}